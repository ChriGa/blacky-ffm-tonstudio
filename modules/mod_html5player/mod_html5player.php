<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_html5player
 *
 * @copyright   Copyright 2013 ATinyTeam. All rights reserved.
 * @license     GNU General Public License version 2 or later
 */

defined('_JEXEC') or die;

require_once __DIR__ . '/helper.php';

ModHtml5playerHelper::addAssets();
$player_mode = $params->get('player_mode', 'single');

$file_source = $params->get('file_source', '');
$file_local = $params->get('file_local', '');
$file_title = $params->get('file_title', '');
$image_source = $params->get('image_source', '');

$playlist_source = $params->get('playlist_source', '');
$load_sample_playlist = $params->get('load_sample_playlist', '0');
$playlist_position = $params->get('playlist_position', 'right');
$playlist_size = $params->get('playlist_size', '180');

$player_width = $params->get('player_width', '640');
$player_height = $params->get('player_height', '360');
$aspectratio = $params->get('aspectratio', '16:9');
$autostart = $params->get('autostart', 'false');
$repeat = $params->get('repeat', 'false');

require JModuleHelper::getLayoutPath('mod_html5player', 'default');
