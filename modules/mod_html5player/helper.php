<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_html5player
 *
 * @copyright   Copyright 2013 ATinyTeam. All rights reserved.
 * @license     GNU General Public License version 2 or later
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_html5player
 *
 * @package     Joomla.Site
 * @subpackage  mod_html5player
 * @since       3.1
 */
class ModHtml5playerHelper
{
	public function addAssets(){
		$document = JFactory::getDocument();
		
		$scripturl = JURI::base()."modules/mod_html5player/asset/jwplayer.js";
		$document->addScript($scripturl);
		
		$cssurl = JURI::base()."modules/mod_html5player/asset/html5player.css";
		$document->addStyleSheet($cssurl);
	}
}
