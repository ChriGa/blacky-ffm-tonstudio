<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_html5player
 *
 * @copyright   Copyright 2013 ATinyTeam. All rights reserved.
 * @license     GNU General Public License version 2 or later
 */

defined('_JEXEC') or die;
$modid = $module->id;
$checksettings = true;
echo '<style>#html5player'.$modid.'_logo{display:none;}</style>';
if($player_mode=="single"){
	if($file_source!="" || $file_local!=""){
	if($file_source==""){
		$file_source = JURI::base().'images/'.$file_local;
	}
	?>
		<div id="html5player<?php echo $modid;?>"></div>
		<script type="text/javascript">
			jwplayer("html5player<?php echo $modid;?>").setup({
				file: "<?php echo $file_source;?>",
				width: "<?php echo $player_width;?>",
				height: "<?php echo $player_height;?>",
				autostart: <?php echo $autostart;?>,
				repeat: <?php echo $repeat;?>
				<?php if($file_title!=""){
					echo ',title: '.'"'.$file_title.'"';
				}?>
				<?php if($image_source!=""){
					echo ',image: '.'"'.JURI::base().$image_source.'"';
				}?>
				<?php
					$aspectratiodimension = explode(":", $aspectratio);
					if( is_numeric($aspectratiodimension[0]) && is_numeric($aspectratiodimension[1]) ){
						echo ',aspectratio: '.'"'.$aspectratio.'"';
					} else {
						echo ',aspectratio: "16:9"';
					}
				?>
			});
		</script>
	<?php } else {$checksettings = false;}
} else {
	if($load_sample_playlist=="1"){
		$playlist_source = JURI::base().'modules/mod_html5player/playlist.rss';
	}
	if($playlist_source!=""){?>
		
		<div id="html5player<?php echo $modid;?>"></div>
		<script type="text/javascript">
			jwplayer("html5player<?php echo $modid;?>").setup({
				playlist: "<?php echo $playlist_source;?>",
				listbar: {
					position: '<?php echo $playlist_position;?>',
					size: <?php echo $playlist_size;?>
				},
				width: "<?php echo $player_width;?>",
				height: "<?php echo $player_height;?>",
				autostart: <?php echo $autostart;?>,
				repeat: <?php echo $repeat;?>
				<?php if($file_title!=""){
					echo ',title: '.'"'.$file_title.'"';
				}?>
				<?php if($image_source!=""){
					echo ',image: '.'"'.$image_source.'"';
				}?>
				<?php
					$aspectratiodimension = explode(":", $aspectratio);
					if( is_numeric($aspectratiodimension[0]) && is_numeric($aspectratiodimension[1]) ){
						echo ',aspectratio: '.'"'.$aspectratio.'"';
					} else {
						echo ',aspectratio: "16:9"';
					}
				?>
			});
		</script>
	<?php } else {$checksettings = false;}?>
<?php
}
if(!$checksettings){
	echo JText::_("MOD_HTML5PLAYER_CHECK_SETTINGS");
}
?>
