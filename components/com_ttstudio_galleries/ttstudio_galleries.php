<?php
/**
 * @version     1.0.0
 * @package     com_ttstudio_galleries
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     http://themeforest.net/licenses
 * @author      Egemen <egemener80@gmail.com> - http://themeforest.net/user/egemenerd
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

// Execute the task.
$controller	= JControllerLegacy::getInstance('Ttstudio_galleries');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
