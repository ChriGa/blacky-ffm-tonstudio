<?php
/**
 * @version     1.0.0
 * @package     com_ttstudio_gallery_items
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     http://themeforest.net/licenses
 * @author      Egemen <egemener80@gmail.com> - http://themeforest.net/user/egemenerd
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Galleryitem controller class.
 */
class Ttstudio_gallery_itemsControllerGalleryitem extends JControllerForm
{

    function __construct() {
        $this->view_list = 'galleryitems';
        parent::__construct();
    }

}