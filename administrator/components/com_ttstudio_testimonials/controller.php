<?php
/**
 * @version     1.0.0
 * @package     com_ttstudio_testimonials
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     http://themeforest.net/licenses
 * @author      Egemen <egemener80@gmail.com> - http://themeforest.net/user/egemenerd
 */


// No direct access
defined('_JEXEC') or die;

class Ttstudio_testimonialsController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param	boolean			$cachable	If true, the view output will be cached
	 * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		require_once JPATH_COMPONENT.'/helpers/ttstudio_testimonials.php';

		$view		= JFactory::getApplication()->input->getCmd('view', 'testimonials');
        JFactory::getApplication()->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}
}
