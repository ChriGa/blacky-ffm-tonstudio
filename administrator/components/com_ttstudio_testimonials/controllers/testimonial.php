<?php
/**
 * @version     1.0.0
 * @package     com_ttstudio_testimonials
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     http://themeforest.net/licenses
 * @author      Egemen <egemener80@gmail.com> - http://themeforest.net/user/egemenerd
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Testimonial controller class.
 */
class Ttstudio_testimonialsControllerTestimonial extends JControllerForm
{

    function __construct() {
        $this->view_list = 'testimonials';
        parent::__construct();
    }

}