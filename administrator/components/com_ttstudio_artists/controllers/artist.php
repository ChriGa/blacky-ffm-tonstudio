<?php
/**
 * @version     1.0.0
 * @package     com_ttstudio_artists
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     http://themeforest.net/licenses
 * @author      Egemen <egemener80@gmail.com> - http://themeforest.net/user/egemenerd
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Artist controller class.
 */
class Ttstudio_artistsControllerArtist extends JControllerForm
{

    function __construct() {
        $this->view_list = 'artists';
        parent::__construct();
    }

}