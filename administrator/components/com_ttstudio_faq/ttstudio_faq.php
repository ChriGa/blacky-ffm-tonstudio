<?php
/**
 * @version     1.0.0
 * @package     com_ttstudio_faq
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     http://themeforest.net/licenses
 * @author      Egemen <egemener80@gmail.com> - http://themeforest.net/user/egemenerd
 */


// no direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_ttstudio_faq')) 
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

$controller	= JControllerLegacy::getInstance('Ttstudio_faq');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
