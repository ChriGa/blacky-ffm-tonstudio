<?php
/**
 * @version     1.0.0
 * @package     com_ttstudio_faq
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     http://themeforest.net/licenses
 * @author      Egemen <egemener80@gmail.com> - http://themeforest.net/user/egemenerd
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Faq controller class.
 */
class Ttstudio_faqControllerFaq extends JControllerForm
{

    function __construct() {
        $this->view_list = 'faqs';
        parent::__construct();
    }

}