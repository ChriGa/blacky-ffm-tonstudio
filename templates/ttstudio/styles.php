<?php
defined( '_JEXEC' ) or die;

$firstcolor = $this->params->get('ttstudio_first_color');
$firstfont = $this->params->get('ttstudio_fontheadingfamily'); 
$secondfont = $this->params->get('ttstudio_fontfamily');  
$loader = $this->params->get('ttstudio_loader'); 
$bloganimation = $this->params->get('ttstudio_bloganimation'); 
?>

<style type="text/css">
.slider_loading, #swipebox-slider .slide {
	background:#000 url('<?php if (!empty($loader)) { echo $loader; } else {echo JUri::base() . 'templates/' . $this->template . '/images/loader.gif';} ?>') no-repeat center center;
}
.grid figcaption {
	width: <?php if (!empty($bloganimation)) { echo $bloganimation . '%'; } else { echo '50%'; } ?>;
}    
h1, h2, h3, h4, h5, h6, header {
	<?php if (!empty($firstfont)) { echo stripslashes($firstfont); } else { echo "font-family: 'oswaldbold';"; } ?>
	font-weight:normal;
} 
#mobile-menu, nav, .gallery figcaption span, .accordion-header, #swipebox-caption, .gallerylink, .result-title
{
    <?php if (!empty($firstfont)) { echo stripslashes($firstfont); } else { echo "font-family: 'oswaldregular';"; } ?>
    font-weight:normal;
}    
body, p, label{
	<?php if (!empty($secondfont)) { echo stripslashes($secondfont); } else { echo "font-family: 'open_sansregular';"; } ?>
    font-weight:normal;
}
strong {
    <?php if (!empty($secondfont)) { echo stripslashes($secondfont); } else { echo "font-family: 'open_sansregular';"; } ?>    
    font-weight: bold;
}
i, em, blockquote cite {
    <?php if (!empty($secondfont)) { echo stripslashes($secondfont); } else { echo "font-family: 'open_sansitalic';"; } ?>
    font-style: italic;
}
::-moz-selection {
	color:#fff;
	text-shadow:none;
	background:<?php if (!empty($firstcolor)) { echo $firstcolor; } else { echo '#aa1111'; } ?>;
}
::selection {
	color:#fff;
	text-shadow:none;
	background:<?php if (!empty($firstcolor)) { echo $firstcolor; } else { echo '#aa1111'; } ?>;
}  
hr {
    background-color:<?php if (!empty($firstcolor)) { echo $firstcolor; } else { echo '#aa1111'; } ?>;
    background: -webkit-linear-gradient(left, #fff, #fff 30%, <?php if (!empty($firstcolor)) { echo $firstcolor; } else { echo '#aa1111'; } ?> 30%, <?php if (!empty($firstcolor)) { echo $firstcolor; } else { echo '#aa1111'; } ?>);
} 
a:hover {
	color: <?php if (!empty($firstcolor)) { echo $firstcolor; } else { echo '#aa1111'; } ?>;
} 
blockquote {
    border-left:3px solid <?php if (!empty($firstcolor)) { echo $firstcolor; } else { echo '#aa1111'; } ?>;
}
nav ul, nav li, .gallery figcaption, .grid figcaption a, .button, input[type="submit"], .comments a.reply:hover, .read-more, .comments_block input[type="submit"], .comments a.comment-reply-link:hover, .da-thumbs li a, .da-thumbs li, .next a:hover, .previous a:hover
{
    background-color:<?php if (!empty($firstcolor)) { echo $firstcolor; } else { echo '#aa1111'; } ?>;
}
.active-header, .active-header:hover
{
    background-color:<?php if (!empty($firstcolor)) { echo $firstcolor . ' !important'; } else { echo '#aa1111 !important'; } ?>;
}
#main-menu ul:after {
	border: solid transparent;
	border-color: transparent;
	border-left-color: <?php if (!empty($firstcolor)) { echo $firstcolor; } else { echo '#aa1111'; } ?>;
    border-width: 30px;
	top: 50%;
	margin-top: -30px;
}
input[type="text"]:focus, input[type="email"]:focus, input[type="number"]:focus, input[type="password"]:focus, input[type="date"]:focus, textarea:focus {
	border-color:<?php if (!empty($firstcolor)) { echo $firstcolor; } else { echo '#aa1111'; } ?>;
}   
@media only screen and (max-width:800px) 
{    
input[type="text"], input[type="email"], input[type="number"], input[type="date"], textarea
{                                                  
    border-color: <?php if (!empty($firstcolor)) { echo $firstcolor . ' !important'; } else { echo '#aa1111 !important'; } ?>;
}
}
</style>