<?php defined( '_JEXEC' ) or die; ?>

<?php if ($socialiconshide == 0) { ?>
<!-- SOCIAL ICONS -->
<ul class="social-icons">
<?php if (!empty($facebook)) { echo '<li><a target="_blank" href="' . $facebook . '">' . '<img class="social-icon" src="' . JUri::base() . 'templates/' . $this->template . '/images/social-icons/facebook.png" alt="" /></a></li>'; } ?>
<?php if (!empty($twitter)) { echo '<li><a target="_blank" href="' . $twitter . '">' . '<img class="social-icon" src="' . JUri::base() . 'templates/' . $this->template . '/images/social-icons/twitter.png" alt="" /></a></li>'; } ?>  
<?php if (!empty($flickr)) { echo '<li><a target="_blank" href="' . $flickr . '">' . '<img class="social-icon" src="' . JUri::base() . 'templates/' . $this->template . '/images/social-icons/flickr.png" alt="" /></a></li>'; } ?>  
<?php if (!empty($linkedin)) { echo '<li><a target="_blank" href="' . $linkedin . '">' . '<img class="social-icon" src="' . JUri::base() . 'templates/' . $this->template . '/images/social-icons/linkedin.png" alt="" /></a></li>'; } ?>  
<?php if (!empty($youtube)) { echo '<li><a target="_blank" href="' . $youtube . '">' . '<img class="social-icon" src="' . JUri::base() . 'templates/' . $this->template . '/images/social-icons/youtube.png" alt="" /></a></li>'; } ?>  
<?php if (!empty($vimeo)) { echo '<li><a target="_blank" href="' . $vimeo . '">' . '<img class="social-icon" src="' . JUri::base() . 'templates/' . $this->template . '/images/social-icons/vimeo.png" alt="" /></a></li>'; } ?>  
<?php if (!empty($google)) { echo '<li><a target="_blank" href="' . $google . '">' . '<img class="social-icon" src="' . JUri::base() . 'templates/' . $this->template . '/images/social-icons/google.png" alt="" /></a></li>'; } ?>  
<?php if (!empty($behance)) { echo '<li><a target="_blank" href="' . $behance . '">' . '<img class="social-icon" src="' . JUri::base() . 'templates/' . $this->template . '/images/social-icons/behance.png" alt="" /></a></li>'; } ?>  
<?php if (!empty($deviantart)) { echo '<li><a target="_blank" href="' . $deviantart . '">' . '<img class="social-icon" src="' . JUri::base() . 'templates/' . $this->template . '/images/social-icons/deviantart.png" alt="" /></a></li>'; } ?>  
<?php if (!empty($pinterest)) { echo '<li><a target="_blank" href="' . $pinterest . '">' . '<img class="social-icon" src="' . JUri::base() . 'templates/' . $this->template . '/images/social-icons/pinterest.png" alt="" /></a></li>'; } ?>  
<?php if (!empty($skype)) { echo '<li><a target="_blank" href="' . $skype . '">' . '<img class="social-icon" src="' . JUri::base() . 'templates/' . $this->template . '/images/social-icons/skype.png" alt="" /></a></li>'; } ?>  
<?php if (!empty($yahoo)) { echo '<li><a target="_blank" href="' . $yahoo . '">' . '<img class="social-icon" src="' . JUri::base() . 'templates/' . $this->template . '/images/social-icons/yahoo.png" alt="" /></a></li>'; } ?>  
<?php if (!empty($customlink)) { echo '<li><a target="_blank" href="' . $customlink . '">' . '<img class="social-icon" src="' . $customiconimg . '" alt="" /></a></li>'; } ?>
<?php if (!empty($secondcustomlink)) { echo '<li><a target="_blank" href="' . $secondcustomlink . '">' . '<img class="social-icon" src="' . $secondcustomiconimg . '" alt="" /></a></li>'; } ?> 
<?php if (!empty($thirdcustomlink)) { echo '<li><a target="_blank" href="' . $thirdcustomlink . '">' . '<img class="social-icon" src="' . $thirdcustomiconimg . '" alt="" /></a></li>'; } ?>     
</ul>
<?php } ?>

<hr/>                       
<h3><?php echo $contactheading; ?></h3>
<form id="contactForm" action="<?php echo JUri::base() . 'templates/' . $this->template . '/processform.php'; ?>" method="post">
    <label>
        <?php if (!empty($namefield)) { echo $namefield; } else { echo 'Name'; } ?>
    </label>
    <input type="text" name="senderName" id="senderName" maxlength="40" />
    <label>
        <?php if (!empty($emailfield)) { echo $emailfield; } else { echo 'Email'; } ?>
    </label>
    <input type="email" name="senderEmail" id="senderEmail" maxlength="50" />
    <label>
        <?php if (!empty($messagefield)) { echo $messagefield; } else { echo 'Message'; } ?>
    </label>
    <textarea name="message" id="message"></textarea>
    <input name="recipientName" id="recipientName" type="hidden" value="<?php echo $contactrecipient; ?>" />
    <input name="recipientSubject" id="recipientSubject" type="hidden" value="<?php echo $contactsubject; ?>" />
    <input name="recipientMail" id="recipientMail" type="hidden" value="<?php echo $contactemail; ?>" />
    <input type="submit" class="button" id="sendMessage" name="sendMessage" value="<?php if (!empty($sendbutton)) { echo $sendbutton; } else { echo 'Send'; } ?>" />
</form>

<?php 

if ($googlemaphide == 0) { ?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>   
<script type="text/javascript">
/////////////////* GOOGLE MAP */////////////////////////

function googlemap() {
    "use strict";      
    google.maps.Map.prototype.setCenterWithOffset= function(latlng, offsetX, offsetY) {
    var map = this;
    var ov = new google.maps.OverlayView();
    ov.onAdd = function() {
        var proj = this.getProjection();
        var aPoint = proj.fromLatLngToContainerPixel(latlng);
        aPoint.x = aPoint.x+offsetX;
        aPoint.y = aPoint.y+offsetY;
        map.setCenter(proj.fromContainerPixelToLatLng(aPoint));
    }; 
    ov.draw = function() {}; 
    ov.setMap(this); 
};
    var latlng = new google.maps.LatLng(<?php echo $maplatitude; ?>, <?php echo $maplongitude; ?>);
    
    var stylez = [
    {
      featureType: "all",
      elementType: "all",
      stylers: [
        { saturation: -100 }
      ]
    }
];

    var myMapOptions = {
        zoom: <?php if (!empty($zoomlevel)) { echo $zoomlevel; } else { echo '17'; } ?>,
        scrollwheel: false,
        disableDefaultUI: true,
        mapTypeControl: true,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.MEDIUM,
            position: google.maps.ControlPosition.LEFT_BOTTOM
        },
        center: latlng,
        mapTypeControlOptions: {
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'tehgrayz']
    }
    };
    
if (jQuery(window).width() > 800) {
    var map = new google.maps.Map(document.getElementById("google-map"), myMapOptions);
    map.setCenterWithOffset(latlng, 200, 0);
}
else {
    var map = new google.maps.Map(document.getElementById("mobile-map"), myMapOptions);
}
    
    var mapType = new google.maps.StyledMapType(stylez, { name:"Grayscale" });    
    map.mapTypes.set('tehgrayz', mapType);
    map.setMapTypeId('tehgrayz');
    var image = '<?php if (!empty($marker)) { echo $marker; } else { echo JUri::base() . 'templates/' . $this->template . '/images/marker.png'; } ?>';

    var marker = new google.maps.Marker({
        draggable: false,
        animation: google.maps.Animation.DROP,
        icon: image,
        map: map,
        position: latlng
    });
}
jQuery(window).load(function () {
    "use strict";
    googlemap();
});
jQuery(window).resize(function () {
    "use strict";
    googlemap();
});      
</script>
<?php } ?>