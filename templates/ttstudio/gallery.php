<?php defined( '_JEXEC' ) or die; ?>
<?php

// Get a db connection.
$gallerydb = JFactory::getDbo();
 
// Create a new query object.
$query = $gallerydb->getQuery(true);
 
$query->select($gallerydb->quoteName(array('id','ttstudio_title','ttstudio_image','ttstudio_orderid','state')));
$query->from($gallerydb->quoteName('#__ttstudio_galleries'));
$query->order($gallerydb->quoteName('ttstudio_orderid'));

// Reset the query using our newly populated query object.
$gallerydb->setQuery($query);
$galleryresults = $gallerydb->loadRowList();

?>

<div class="gallery">
<?php foreach ($galleryresults as $row) : ?> 
<?php if ($row[4] == 1) { ?> 
<?php if ($portfolioimghide == 0) { ?>     
    <figure>
        <a class="gallery<?php echo $row[0]; ?>" data-title="<?php echo $row[1]; ?>" href="<?php echo $row[2]; ?>">
            <img src="<?php echo $row[2]; ?>" alt="">
        </a>
        <figcaption>
            <span><?php echo $row[1]; ?></span>
        </figcaption>
    </figure>
<?php } else { ?>  
    <a class="gallerylink gallery<?php echo $row[0]; ?>" href="<?php echo $row[2]; ?>" data-title="<?php echo $row[1]; ?>"><?php echo $row[1]; ?></a><br/>
<?php } ?>    
<script type="text/javascript">
jQuery(function($) {
	$(".gallery<?php echo $row[0]; ?>").swipebox({hideBarsDelay : <?php if (!empty($swipeboxautohide)) { echo $swipeboxautohide . '000'; } else { echo '0'; } ?>});
});
</script>    
<?php } ?>    
<?php endforeach; ?>    
</div>

<?php

// Get a db connection.
$itemsdb = JFactory::getDbo();
 
// Create a new query object.
$query = $itemsdb->getQuery(true);
 
$query->select($itemsdb->quoteName(array('id','ttstudio_title','ttstudio_gallery','ttstudio_image','ttstudio_video','ttstudio_orderid','state')));
$query->from($itemsdb->quoteName('#__ttstudio_gallery_items'));
$query->order($itemsdb->quoteName('ttstudio_orderid'));

// Reset the query using our newly populated query object.
$itemsdb->setQuery($query);
$itemsresults = $itemsdb->loadRowList();

?>

<?php foreach ($itemsresults as $row) :
if ($row[6] == 1) {
echo '<a data-rel="gallery' . $row[2] . '" href="';
if (!empty($row[4])) { echo $row[4]; } else { echo $row[3]; }
echo '" class="hide gallery' . $row[2] . '" data-title="' . $row[1] . '"></a>';
}
endforeach; ?>