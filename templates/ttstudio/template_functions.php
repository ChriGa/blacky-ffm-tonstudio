<?php

/**
 * Hier werden Hilfsfunktionen und Variablen definiert die Systemweit durch include
 * in der index.php verwendbar und aufrufbar sind
 * Author: CG
 **/

/**
 * Debuggingausgabe von Objekten und Arrays
 **/	
	function preprint($s, $where="") {
		print "<pre>";
			if ($where) print $where."<br/>";
			print_r($s);
		print "</pre>";
	}

// Sind wir auf der Startseit?: http://docs.joomla.org/How_to_determine_if_the_user_is_viewing_the_front_page
$menu = JFactory::getApplication()->getMenu();
$currentMenuID = $menu -> getActive()->id;
//print_r("Menue ist $currentMenuID");
$body_class = " is_home";
	if ($currentMenuID != 111 ) { $body_class = " not_home"; }


/**
 * Meta Generator löschen und 089-webdesign eintragen
 **/
	unset($headMetaData["metaTags"]["standard"]["title"]); 
	$this->setGenerator('Chris Gabler Web-Development WEB-LOVES-YOU'); 

?>