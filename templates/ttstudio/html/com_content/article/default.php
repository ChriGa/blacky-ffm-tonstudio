<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.ttstudio
 *
 * @copyright   Copyright (C) 2013 Egemenerd
 */

defined('_JEXEC') or die;


$images = json_decode($this->item->images);
$urls = json_decode($this->item->urls);
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
JHtml::_('behavior.caption');

$params = $this->item->params;

?>

                <?php if ($params->get('show_title')) { ?>
                <h1><?php echo $this->escape($this->item->title); ?></h1>
                <hr/>
<?php } ?>
<?php $useDefList = (($params->get('show_author')) or ($params->get('show_category')) or ($params->get('show_parent_category'))
	or ($params->get('show_create_date')) or ($params->get('show_modify_date')) or ($params->get('show_publish_date'))
	or ($params->get('show_hits'))); ?>
<?php if ($useDefList) : ?>
<?php if (isset($images->image_intro) and !empty($images->image_intro)) : ?>
    <ul class="grid">
        <li>
            <figure>
                <div>
                    <img <?php if ($images->image_intro_caption): echo 'class="caption"'.' title="' .htmlspecialchars($images->image_intro_caption) .'"'; endif; ?>
		src="<?php echo htmlspecialchars($images->image_intro); ?>" alt="<?php echo htmlspecialchars($images->image_intro_alt); ?>"/>
                </div>
                <figcaption>
                    <div class="dateicon">
<?php if ($params->get('show_create_date')) : ?>
		<?php echo JText::sprintf(JHtml::_('date', $this->item->created, JText::_('DATE_FORMAT_LC2'))); ?>
<?php endif; ?>
<?php if ($params->get('show_modify_date')) : ?>
		<?php echo JText::sprintf(JHtml::_('date', $this->item->modified, JText::_('DATE_FORMAT_LC2'))); ?>
<?php endif; ?>
<?php if ($params->get('show_publish_date')) : ?>
		<?php echo JText::sprintf(JHtml::_('date', $this->item->publish_up, JText::_('DATE_FORMAT_LC2'))); ?>
<?php endif; ?>                     
                    </div>
<?php $author = $this->item->author; ?>
<?php $author = ($this->item->created_by_alias ? $this->item->created_by_alias : $author);?>
<?php if ($params->get('show_author')) : ?>                    
                    <div class="usericon"><?php echo JText::sprintf('COM_CONTENT_WRITTEN_BY', $author); ?></div>
<?php endif; ?>                    
<?php if ($params->get('show_category')) : ?>
                    <div class="categoryicon">
<?php $categorytitle = $this->escape($this->item->category_title);
$categoryurl = '<a href="'.JRoute::_(ContentHelperRoute::getCategoryRoute($this->item->catslug)).'">'.$categorytitle.'</a>'; ?>
<?php if ($params->get('link_category') and $this->item->catslug) : ?>
<?php echo JText::sprintf('COM_CONTENT_CATEGORY', $categoryurl); ?>
<?php else : ?>
<?php echo JText::sprintf('COM_CONTENT_CATEGORY', $categorytitle); ?>
<?php endif; ?>                        
                    </div>
                    <?php endif; ?>
<?php if ($params->get('show_hits')) : ?>                     
                    <div class="tagsicon"><?php echo JText::sprintf('COM_CONTENT_ARTICLE_HITS', $this->item->hits); ?></div>
<?php endif; ?>           
                </figcaption>
            </figure>
        </li>
    </ul> 
<?php endif; ?>
<?php endif; ?>
<?php if (isset($images->image_intro) and !empty($images->image_intro)) : ?>    
    <div class="blog-content">
<?php else : ?>
    <div>
<?php endif; ?>
                           <?php
    if (!empty($this->item->pagination) && $this->item->pagination && !$this->item->paginationposition && $this->item->paginationrelative)
{
	echo $this->item->pagination;
}

                            ?>
                            
<?php  if (!$params->get('show_intro')) :
echo $this->item->event->afterDisplayTitle;
endif; ?>
                            
<?php echo $this->item->event->beforeDisplayContent; ?>
                         
            <?php echo $this->item->text; ?>
         <div class="clr"></div>
	<?php if (isset ($this->item->toc)) : ?>
		<?php echo $this->item->toc; ?>
	<?php endif; ?>

<?php if (isset($urls) AND ((!empty($urls->urls_position) AND ($urls->urls_position == '0')) OR ($params->get('urls_position') == '0' AND empty($urls->urls_position)))
		OR (empty($urls->urls_position) AND (!$params->get('urls_position')))) : ?>

	<?php echo $this->loadTemplate('links'); ?>
<?php endif; ?>                            
<?php
if (!empty($this->item->pagination) AND $this->item->pagination AND $this->item->paginationposition AND!$this->item->paginationrelative):
	echo $this->item->pagination;?>
<?php endif; ?>

	<?php if (isset($urls) AND ((!empty($urls->urls_position) AND ($urls->urls_position == '1')) OR ( $params->get('urls_position') == '1'))) : ?>

	<?php echo $this->loadTemplate('links'); ?>
	<?php endif; ?>
<?php
if (!empty($this->item->pagination) AND $this->item->pagination AND $this->item->paginationposition AND $this->item->paginationrelative):
	echo $this->item->pagination;?>
<?php endif; ?>
<div class="clr"></div>


	<?php echo $this->item->event->afterDisplayContent; ?>        
        </div>
            </div>