<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.ttstudio
 *
 * @copyright   Copyright (C) 2013 Egemenerd
 */

defined('_JEXEC') or die;

?>

<h4><?php echo JText::_('COM_CONTENT_MORE_ARTICLES'); ?></h4>

<ol class="links">
<?php foreach ($this->link_items as &$item) : ?>
	<li>
		<a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catslug)); ?>">
			<?php echo $item->title; ?></a>
	</li>
<?php endforeach; ?>
</ol>
<div class="clr"></div>

