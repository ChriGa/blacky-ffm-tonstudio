<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.ttstudio
 *
 * @copyright   Copyright (C) 2013 Egemenerd
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication();
$images = json_decode($this->item->images);
$urls = json_decode($this->item->urls);
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
JHtml::_('behavior.caption');

$params = $this->item->params;
?>

<?php if ($this->params->get('show_category_title')) : ?>
	<?php echo '<h1>'.$this->category->title.'</h1><hr/>'; ?>
<?php endif; ?>

<div class="blog<?php echo $this->pageclass_sfx;?>">

		<?php 
		/**
		* manueller override CG für Menue h1 auf Kategorie Blogübersicht
		**/

			echo '<h1>'.$this->menu->title.'</h1><hr/>'; 
		?>


<?php if ($this->params->get('show_description', 1) || $this->params->def('show_description_image', 1)) : ?>
	<div class="category-desc">
	<?php if ($this->params->get('show_description_image') && $this->category->getParams()->get('image')) : ?>
		<img src="<?php echo $this->category->getParams()->get('image'); ?>"/>
	<?php endif; ?>
	<?php if ($this->params->get('show_description') && $this->category->description) : ?>
		<?php echo JHtml::_('content.prepare', $this->category->description, '', 'com_content.category'); ?>
	<?php endif; ?>
	<div class="clr"></div>
	</div>
<?php endif; ?>



<?php $leadingcount = 0; ?>
<?php if (!empty($this->lead_items)) : ?>
<div class="items-leading">
	<?php foreach ($this->lead_items as &$item) : ?>
		<div class="leading-<?php echo $leadingcount; ?><?php echo $item->state == 0 ? 'system-unpublished' : null; ?>">
			<?php
				$this->item = &$item;
				echo $this->loadTemplate('item');
			?>
		</div>
		<?php
			$leadingcount++;
		?>
	<?php endforeach; ?>
</div>
<?php endif; ?>
<?php
	$introcount = (count($this->intro_items));
	$counter = 0;
?>
<?php if (!empty($this->intro_items)) : ?>

	<?php foreach ($this->intro_items as $key => &$item) : ?>

	<div class="<?php echo $item->state == 0 ? ' system-unpublished' : null; ?>">
		<?php
			$this->item = &$item;
			echo $this->loadTemplate('item');
		?>
	</div>

	<?php endforeach; ?>


<?php endif; ?>

<?php if (!empty($this->link_items)) : ?>
<div class="items-more">
	<?php echo $this->loadTemplate('links'); ?>
	</div>
    <hr/>
<?php endif; ?>

  	
	<?php if (is_array($this->children[$this->category->id]) && count($this->children[$this->category->id]) > 0 && $this->params->get('maxLevel') != 0) : ?>
<div class="cat-children">
		<?php if ($this->params->get('show_category_heading_title_text', 1) == 1) : ?>
		<h5>
			<?php echo JTEXT::_('JGLOBAL_SUBCATEGORIES'); ?>
		</h5>
		<?php endif; ?>
	
			<?php echo $this->loadTemplate('children'); ?>
		</div>
<?php endif; ?>

<?php if (($this->params->def('show_pagination', 1) == 1  || ($this->params->get('show_pagination') == 2)) && ($this->pagination->pagesTotal > 1)) : ?>
		<div class="pagination">
						<?php  if ($this->params->def('show_pagination_results', 1)) : ?>
						<span class="counter">
								<?php echo $this->pagination->getPagesCounter(); ?>
						</span>

				<?php endif; ?>
				<?php echo $this->pagination->getPagesLinks(); ?>
		</div>
<?php  endif; ?>
<div class="clr"></div>
</div>