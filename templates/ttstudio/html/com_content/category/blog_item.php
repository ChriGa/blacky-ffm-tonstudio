<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.ttstudio
 *
 * @copyright   Copyright (C) 2013 Egemenerd
 */

defined('_JEXEC') or die;

// Create a shortcut for params.
$canEdit = $this->item->params->get('access-edit');
$params = &$this->item->params;
$images = json_decode($this->item->images);
$app = JFactory::getApplication();
$templateparams = $app->getTemplate(true)->params;

?>

<?php if ($this->item->state == 0) : ?>
<div class="system-unpublished">
<?php endif; ?>   
    
<?php if (!$params->get('show_intro')) : ?>
	<?php echo $this->item->event->afterDisplayTitle; ?>
<?php endif; ?>

<?php echo $this->item->event->beforeDisplayContent; ?>

<?php if (isset($images->image_intro) and !empty($images->image_intro)) : ?>
    <ul class="grid">
        <li>
            <figure>
                <div>
                    <img <?php if ($images->image_intro_caption): echo 'class="caption"'.' title="' .htmlspecialchars($images->image_intro_caption) .'"'; endif; ?>
		src="<?php echo htmlspecialchars($images->image_intro); ?>" alt="<?php echo htmlspecialchars($images->image_intro_alt); ?>"/>
                </div>
                <figcaption>
                    <div class="dateicon">
<?php if ($params->get('show_create_date')) : ?>
		<?php echo JText::sprintf(JHtml::_('date', $this->item->created, JText::_('DATE_FORMAT_LC2'))); ?>
<?php endif; ?>
<?php if ($params->get('show_modify_date')) : ?>
		<?php echo JText::sprintf(JHtml::_('date', $this->item->modified, JText::_('DATE_FORMAT_LC2'))); ?>
<?php endif; ?>
<?php if ($params->get('show_publish_date')) : ?>
		<?php echo JText::sprintf(JHtml::_('date', $this->item->publish_up, JText::_('DATE_FORMAT_LC2'))); ?>
<?php endif; ?>                     
                    </div>
<?php $author = $this->item->author; ?>
<?php $author = ($this->item->created_by_alias ? $this->item->created_by_alias : $author);?>
<?php if ($params->get('show_author')) : ?>                    
                    <div class="usericon"><?php echo JText::sprintf('COM_CONTENT_WRITTEN_BY', $author); ?></div>
<?php endif; ?>                    
<?php if ($params->get('show_category')) : ?>
                    <div class="categoryicon">
<?php $categorytitle = $this->escape($this->item->category_title);
$categoryurl = '<a href="'.JRoute::_(ContentHelperRoute::getCategoryRoute($this->item->catslug)).'">'.$categorytitle.'</a>'; ?>
<?php if ($params->get('link_category') and $this->item->catslug) : ?>
<?php echo JText::sprintf('COM_CONTENT_CATEGORY', $categoryurl); ?>
<?php else : ?>
<?php echo JText::sprintf('COM_CONTENT_CATEGORY', $categorytitle); ?>
<?php endif; ?>                        
                    </div>
                    <?php endif; ?>
<?php if ($params->get('show_hits')) : ?>                     
                    <div class="tagsicon"><?php echo JText::sprintf('COM_CONTENT_ARTICLE_HITS', $this->item->hits); ?></div>
<?php endif; ?>
                    <?php if ($params->get('show_readmore')) { ?>
                    <a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid)); ?>"><?php echo JText::sprintf('COM_CONTENT_READ_MORE_TITLE'); ?></a>
                    <?php } ?>
                </figcaption>
            </figure>
        </li>
    </ul>    
<?php endif; ?>
<?php if (isset($images->image_intro) and !empty($images->image_intro)) : ?>    
    <div class="blog-content">
<?php else : ?>
    <div>
<?php endif; ?>    
     <h3>
<?php if ($params->get('link_titles') && $params->get('access-view')) : ?>
			<a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid)); ?>">
			<?php echo $this->escape($this->item->title); ?></a>
		<?php else : ?>
			<?php echo $this->escape($this->item->title); ?>
		<?php endif; ?>
     </h3>
    </div>
<?php echo $this->item->introtext; ?>
<?php if ($params->get('show_readmore') && $this->item->readmore) : 
	if ($params->get('access-view')) :
		$link = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
	else :
		$menu = JFactory::getApplication()->getMenu();
		$active = $menu->getActive();
		$itemId = $active->id;
		$link1 = JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId);
		$returnURL = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
		$link = new JUri($link1);
		$link->setVar('return', base64_encode($returnURL));
	endif;        
        ?>    
    <a class="read-more" href="<?php echo $link; ?>">
					<?php if (!$params->get('access-view')) :
						echo JText::_('COM_CONTENT_REGISTER_TO_READ_MORE');
					elseif ($readmore = $this->item->alternative_readmore) :
						echo $readmore;
						if ($params->get('show_readmore_title', 0) != 0) :
							echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
						endif;
					elseif ($params->get('show_readmore_title', 0) == 0) :
						echo JText::sprintf('COM_CONTENT_READ_MORE_TITLE');
					else :
						echo JText::_('COM_CONTENT_READ_MORE');
						echo JHtml::_('string.truncate', ($this->item->title), $params->get('readmore_limit'));
					endif; ?>
    </a>
        <hr/>
<?php endif; ?>
<?php echo $this->item->event->afterDisplayContent; ?>