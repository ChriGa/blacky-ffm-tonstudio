<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.rock4life
 *
 * @copyright   Copyright (C) 2013 Egemenerd
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication();
$templateparams = $app->getTemplate(true)->params;
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
JHtml::_('behavior.caption');

?>

<?php if ($this->params->get('show_page_heading')) : ?>
	<h1>
	<?php echo $this->escape($this->params->get('page_heading')); ?>
	</h1>
<hr/>
<?php endif; ?> 

<div class="archive<?php echo $this->pageclass_sfx;?>">
<form id="adminForm" class="archive-form" action="<?php echo JRoute::_('index.php')?>" method="post">
    <h3><?php echo JText::_('JGLOBAL_FILTER_LABEL'); ?></h3>
	<div class="filter-search">
		<?php if ($this->params->get('filter_field') != 'hide') : ?>
		<label class="filter-search-lbl" for="filter-search"><?php echo JText::_('COM_CONTENT_'.$this->params->get('filter_field').'_FILTER_LABEL').'&#160;'; ?></label>
		<input type="text" name="filter-search" id="filter-search" value="<?php echo $this->escape($this->filter); ?>" class="inputbox" onchange="document.getElementById('adminForm').submit();" />
		<?php endif; ?>

		<?php echo $this->form->monthField; ?>
		<?php echo $this->form->yearField; ?>
		<?php echo $this->form->limitField; ?>
		<div class="archive-button"><button type="submit" class="button"><?php echo JText::_('JGLOBAL_FILTER_BUTTON'); ?></button></div>
	</div>
	<input type="hidden" name="view" value="archive" />
	<input type="hidden" name="option" value="com_content" />
	<input type="hidden" name="limitstart" value="0" />
<hr/>
	<?php echo $this->loadTemplate('items'); ?>
</form>
<div class="clr"></div>     
</div>  
