<?php defined( '_JEXEC' ) or die; 

$doc = JFactory::getDocument();
$app = JFactory::getApplication();
$tpath = $this->baseurl.'templates/'.$this->template;


?><!doctype html>

<html class="no-js" lang="<?php echo $this->language; ?>">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <link rel="stylesheet" href="<?php echo JUri::base() . $tpath; ?>/css/error.css"> 
  <!--[if lte IE 8]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  </head>
<body>
    
<section id="ascensorBuilding">
    <article>      
            <div class="content">
<h1><?php echo $this->error->getCode().' - '.$this->error->getMessage();  ?></h1> 
                <hr/>
                        <h4>
                            <?php 
if (($this->error->getCode()) == '404') {
    echo JText::_('JERROR_LAYOUT_REQUESTED_RESOURCE_WAS_NOT_FOUND');
}
                            ?>
<?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>: 
                            <a href="<?php echo $this->baseurl; ?>/"><?php echo JText::_('JERROR_LAYOUT_HOME_PAGE'); ?></a>.
                        </h4> 
<div class="searchbox">              
                            <?php
$module = new stdClass();
$module->module = 'mod_search';
echo JModuleHelper::renderModule($module);
                            ?>
</div>                  
                </div>
        </article>
    </section>   
    </body>
    </html>