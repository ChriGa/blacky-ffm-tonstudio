<?php defined( '_JEXEC' ) or die; 

$doc = JFactory::getDocument();

$activate = $this->params->get('ttstudio_activate');
$searchbox = $this->params->get('ttstudio_searchbox');
$favicon = $this->params->get('ttstudio_favicon');
$customcode = $this->params->get('ttstudio_customcode');
$firstfontlink = $this->params->get('ttstudio_titlefontlink'); 
$secondfontlink = $this->params->get('ttstudio_fontlink');
$headerimage = $this->params->get('ttstudio_logo'); 
$mobilemenu = $this->params->get('ttstudio_mobilemenu');
$homemenu = $this->params->get('ttstudio_homemenu'); 
$bgimage  = $this->params->get('ttstudio_innerbg');
$footertext = $this->params->get('ttstudio_footermessage');

$doc->addStyleSheet(JUri::base() . 'templates/system/css/system.css');
$doc->addStyleSheet(JUri::base() . 'templates/' . $this->template . '/css/style.css', $type = 'text/css');
 
$doc->addScript(JUri::base() . 'templates/' . $this->template . '/js/modernizr.custom.js', 'text/javascript');
JHtml::_('jquery.framework', 'text/javascript');
$doc->addScript(JUri::base() . 'templates/' . $this->template . '/js/backstretch.min.js', 'text/javascript');
$doc->addScript(JUri::base() . 'templates/' . $this->template . '/js/jquery.swipebox.min.js', 'text/javascript');

//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');

?><!doctype html>
<html class="no-js" lang="<?php echo $this->language; ?>">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <jdoc:include type="head" />
  <link rel="shortcut icon" href="<?php if (!empty($favicon)) { echo $favicon; } else { echo JUri::base() . 'templates/' . $this->template . '/images/favicon.ico'; } ?>" type="image/x-icon" />
    <?php
if (!empty($firstfontlink)) { echo stripslashes($firstfontlink); }
if (!empty($secondfontlink)) { echo stripslashes($secondfontlink); }   
    ?>    
  <?php if (!empty($customcode)) { echo stripslashes($customcode); } ?> 
  <!--[if lte IE 8]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
    <?php 
require __DIR__ . '/styles.php';
function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } 
    else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

$pagelink = curPageURL(); 
    ?>
    
    <!--CG final -> Template settings-->
    <!--<link rel="stylesheet" type="text/css" href="http://ffm-tonstudio.de/templates/ttstudio/css/overrides.css" />-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58685073-1', 'auto');
  ga('send', 'pageview');

</script>

  <!-- scripts include -->
    <script src="/templates/ttstudio/js/scripts.js" type="text/javascript"></script>

</head>

<body>   
    <!-- HEADER  -->
    <header>
<?php if (!empty($headerimage)) { echo '<img src="' . $headerimage . '" alt="" />'; } else { echo '<img src="' . JUri::base() . 'templates/' . $this->template . '/images/logo.png" alt="" />'; } ?>
    </header>    
    
<?php

if (($activate == 1) && ($pagelink == JUri::base())) {
    include_once __DIR__ . '/frontpage.php';
    if (!empty($footertext)) { echo '<footer>' . stripslashes($footertext) . '</footer>'; }
    echo '</body>'; 
    echo '</html>'; 
    } 
else { 
?>


<div id="mobile-menu"><?php if (!empty($mobilemenu)) { echo $mobilemenu; } else { echo 'Menu'; } ?></div>
<nav id="main-menu">
<jdoc:include type="modules" name="submenu" />
</nav>


<section id="ascensorBuilding">
    <article>
        <div class="page-content" data-stellar-ratio="2">         
            <div class="content">
        <jdoc:include type="component" />

        <!-- FBshareButton -->
            <?  if ($this->countModules("facebook")) { ?>                   
                <div class="clear"></div>
                <div id="fbLike">
                    <jdoc:include type="modules" name="facebook" style="xhtml_id"  />
                </div>
                <? } ?>
                 
            </div>
        </div>
    </article>
</section>



<?php if (!empty($footertext)) { echo '<footer>' . stripslashes($footertext) . '</footer>'; } ?>

<script type="text/javascript" src="<?php echo JUri::base() . 'templates/' . $this->template . '/js/custom2.js' ?>"></script> 
<script type="text/javascript" src="<?php echo JUri::base() . 'templates/' . $this->template . '/js/toucheffects.js' ?>"></script> 
<?php if (!empty($bgimage)) { echo '<script type="text/javascript"> jQuery("body").backstretch("/' . $bgimage . '"); </script>'; } ?>    
<jdoc:include type="modules" name="debug" /> 

<?
/**
* Content Menue BCKgr Image div nach Menu ID
**/
    $currentMenuId = JSite::getMenu()->getActive()->id;
    //print $currentMenuId;

    if ($currentMenuId == 130)
        print '<div class="cont_back backstretch">
                <img src="images/studio_bilder/backgr_content_menu/DSC_9585.jpg" alt="Black-Keys Studio Equipment"/>
                </div>';
    else if ($currentMenuId == 131)
        print '<div class="cont_back backstretch">
                    <img src="images/studio_bilder/backgr_content_menu/Harmonium-DSC_9761.jpg" alt="Black-Keys Studio Blog"/>
                </div>';
    else if ($currentMenuId == 133)
        print '<div class="cont_back backstretch">
                <img src="images/studio_bilder/backgr_content_menu/Keyboard-DSC_9639.jpg" alt="Black-Keys Studio Projekte"/>
                </div>';
    else  if ($currentMenuId == 137)
        print '<div class="cont_back backstretch">
                <img src="images/studio_bilder/backgr_content_menu/channels_DSC_9647.jpg" alt="Black-Keys Studio Projekte"/>
                </div>';
    else if ($currentMenuId == 139)
        print '<div class="cont_back backstretch">
                <img src="images/studio_bilder/studio_desk_1.jpg" alt="Black-Keys Studio Projekte"/>
                </div>';
    else 
        print '<div class="cont_back backstretch">
                <img src="images/studio_bilder/backgr_content_menu/Keyboard-DSC_9639.jpg" alt="Black-Keys Studio Projekte"/>
                </div>';

?>


<? /*<style type="text/css">
.fbLike {
    position: absolute;
    bottom: 10;
    float: right;
    border: 5px solid red;
}

</style>
<div class="fbLike">
    <p><a href="javascript:fbShare(window.location.href, 'fb_share', 'Teilen auf Facebook', 520, 350)"><img src="/images/fb_thumb.png" alt="Auf Facebook teilen" /></a></p>
</div>
*/ ?>

</body>

</html>      
<?php } ?> 

