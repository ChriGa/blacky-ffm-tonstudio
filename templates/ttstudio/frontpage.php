<?php
defined('_JEXEC') or die;

$autoplay = $this->params->get('ttstudio_sliderautoplay');
$duration = $this->params->get('ttstudio_sliderduration');
$testimonialstitle = $this->params->get('ttstudio_testimonialstitle');
$testimonialsdelay = $this->params->get('ttstudio_testimonialsdelay');
$testimonialshide  = $this->params->get('ttstudio_testimonialshide');
$googlemaphide = $this->params->get('ttstudio_googlemaphide');
$contactheading = $this->params->get('ttstudio_contactform_header');
$contactformhide = $this->params->get('ttstudio_contactformhide');
$namefield = $this->params->get('ttstudio_name_field');
$emailfield = $this->params->get('ttstudio_email_field');
$messagefield = $this->params->get('ttstudio_message_field');
$sendbutton = $this->params->get('ttstudio_send_button');
$contactrecipient = $this->params->get('ttstudio_contactform_recipient');
$contactsubject = $this->params->get('ttstudio_contactform_subject');
$contactemail = $this->params->get('ttstudio_contactform_email');
$zoomlevel = $this->params->get('ttstudio_mapzoom');
$maplatitude = $this->params->get('ttstudio_maplatitude');
$maplongitude = $this->params->get('ttstudio_maplongitude');
$marker = $this->params->get('ttstudio_marker');
$socialiconshide = $this->params->get('ttstudio_hide_sociallist');
$facebook = $this->params->get('ttstudio_facebook_uid');
$twitter = $this->params->get('ttstudio_twitter_uid');
$flickr = $this->params->get('ttstudio_flickr_uid');
$linkedin = $this->params->get('ttstudio_linkedin_uid');
$youtube = $this->params->get('ttstudio_youtube_uid');
$vimeo = $this->params->get('ttstudio_vimeo_uid');
$google = $this->params->get('ttstudio_google_uid');
$behance = $this->params->get('ttstudio_behance_uid');
$deviantart = $this->params->get('ttstudio_deviantart_uid');
$pinterest = $this->params->get('ttstudio_pinterest_uid');
$skype = $this->params->get('ttstudio_skype_uid');
$yahoo = $this->params->get('ttstudio_yahoo_uid');
$customiconimg = $this->params->get('ttstudio_customicon');
$customlink = $this->params->get('ttstudio_custom_uid');
$secondcustomiconimg = $this->params->get('ttstudio_secondcustomicon');
$secondcustomlink = $this->params->get('ttstudio_secondcustom_uid');
$thirdcustomiconimg = $this->params->get('ttstudio_thirdcustomicon');
$thirdcustomlink = $this->params->get('ttstudio_thirdcustom_uid');
$artisthide = $this->params->get('ttstudio_artistshide');
$portfoliotext = $this->params->get('ttstudio_portfoliobutton');
$swipeboxautohide = $this->params->get('ttstudio_swipeboxautohide');
$portfolioimghide = $this->params->get('ttstudio_portfolioimghide');

?>   
<style type="text/css">nav .current { display:none; }</style>
<div id="mobile-menu"><?php if (!empty($mobilemenu)) { echo $mobilemenu; } else { echo 'Menu'; } ?></div>

<nav id="main-menu">
    <ul>
        <li>
            <a class="ascensorLink ascensorLink0"><?php if (!empty($homemenu)) { echo $homemenu; } else { echo 'Home'; } ?></a>
        </li>
<?php

// Get a db connection.
$db = JFactory::getDbo();
 
// Create a new query object.
$query = $db->getQuery(true);
 
$query->select($db->quoteName(array('id','ttstudio_orderid','ttstudio_menutitle','ttstudio_pagetype','ttstudio_bgimage','ttstudio_content','state')));
$query->from($db->quoteName('#__ttstudio_frontpage'));
$query->order($db->quoteName('ttstudio_orderid'));

$count = 1;
$imagecount = 1;
 
// Reset the query using our newly populated query object.
$db->setQuery($query);
$results = $db->loadRowList();

foreach ($results as $row) :

if ($row[6] == 1) {

echo '<li>';
echo '<a class="ascensorLink ascensorLink' . $count++ . '">' . $row[2] . '</a>';
echo '</li>';

}    
    
endforeach; ?>
    </ul>
</nav>
<nav id="sub-menu">
<jdoc:include type="modules" name="submenu" />
</nav>
<!-- PAGES -->
        <section id="ascensorBuilding">
            
<!-- SLIDER -->            
<?php include_once __DIR__ . '/slider.php'; ?>
            
<!-- OTHER PAGES -->            
<?php foreach ($results as $row) :

// Modul


// If it is a standard large page
if (($row[3] == 1) && ($row[6] == 1))
{ 
    echo '<article><div class="page-content" data-stellar-ratio="2"><div class="content">' . $row[5] . '</div>
    </div>
        </article>';
}

// If it is a standard small page
if (($row[3] == 2) && ($row[6] == 1))
{ 
    echo '<article><div class="page-content-small" data-stellar-ratio="2"><div class="content">' . $row[5] . '</div></div></article>';   
}

// If it is an about us page
if (($row[3] == 3) && ($row[6] == 1))
{ 
    echo '<article><div class="page-content" data-stellar-ratio="2"><div class="content">';
    echo $row[5];
    include_once __DIR__ . '/testimonials.php';
    echo '</div></div></article>';   
}

// If it is an artists page
if (($row[3] == 4) && ($row[6] == 1))
{ 
    echo '<article><div class="page-content-small" data-stellar-ratio="2"><div class="content">';
    echo $row[5];
    include_once __DIR__ . '/artists.php';
    echo '</div></div></article>';   
}

// If it is a faq page
if (($row[3] == 5) && ($row[6] == 1))
{ 
    echo '<article><div class="page-content-small" data-stellar-ratio="2"><div class="content">';
    echo $row[5];
    include_once __DIR__ . '/faq.php';
    echo '</div></div></article>';   
}

// If it is a contact page
if (($row[3] == 6) && ($row[6] == 1))
{
    echo '<article>';
    if ($googlemaphide == 0) { echo '<div id="google-map" class="google-map"></div>'; }
    echo '<div class="page-content-small" data-stellar-ratio="2"><div class="content">';
    if ($googlemaphide == 0) { echo '<div id="mobile-map"></div>'; }
    echo $row[5];
    include_once __DIR__ . '/contact.php';
    echo '</div></div></article>';   
}

// If it is a gallery page
if (($row[3] == 7) && ($row[6] == 1))
{ 
    echo '<article><div class="page-content-small" data-stellar-ratio="2"><div class="content">';
    echo $row[5];
    include_once __DIR__ . '/gallery.php';
    echo '</div></div></article>';   
}

endforeach; ?>
            
        </section>
<script type="text/javascript" src="<?php echo JUri::base() . 'templates/' . $this->template . '/js/jquery.ascensor.js' ?>"></script> 
<script type="text/javascript" src="<?php echo JUri::base() . 'templates/' . $this->template . '/js/slider.js' ?>"></script> 
<script type="text/javascript" src="<?php echo JUri::base() . 'templates/' . $this->template . '/js/stellar.js' ?>"></script>
<script type="text/javascript" src="<?php echo JUri::base() . 'templates/' . $this->template . '/js/custom.js' ?>"></script> 
<script type="text/javascript" src="<?php echo JUri::base() . 'templates/' . $this->template . '/js/toucheffects.js' ?>"></script> 

<?php foreach ($results as $row) :

if ($row[6] == 1) {    
echo '<script type="text/javascript">';
echo 'jQuery("#ascensorFloor' . $imagecount++ . '").backstretch("/' . $row[4] . '");';
echo '</script>';
}

endforeach; ?>