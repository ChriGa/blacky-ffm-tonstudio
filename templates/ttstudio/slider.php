<?php defined( '_JEXEC' ) or die; ?> 
<?php

// Get a db connection.
$sliderdb = JFactory::getDbo();
 
// Create a new query object.
$query = $sliderdb->getQuery(true);
 
$query->select($sliderdb->quoteName(array('id','ttstudio_title','ttstudio_image','ttstudio_orderid','state')));
$query->from($sliderdb->quoteName('#__ttstudio_slider'));
$query->order($sliderdb->quoteName('ttstudio_orderid'));

// Reset the query using our newly populated query object.
$sliderdb->setQuery($query);
$sliderresults = $sliderdb->loadRowList();

?>
<!-- LOADING IMAGE -->
<div id="slider_loading" class="slider_loading"></div> 
            <article>
                <!-- SLIDER -->
                <div id="slider_bg" class="slider_bg">          
<?php foreach ($sliderresults as $row) :
if ($row[4] == 1) {
echo '<img src="' . $row[2] . '" alt="" />';
}
endforeach; ?>
                </div>
                <div id="slider_content_wrapper" class="slider_content_wrapper">
<?php foreach ($sliderresults as $row) :
if ($row[4] == 1) {
$c++;

if ( $c == 1 ) { $class .= ' fadeInLeft showcontent'; } else { $class = ''; };

echo '<div class="slider_content animated' . $class . '" id="content' . $row[0] . '"><h2>' . $row[1] . '</h2></div>';
}
endforeach; ?>
                </div>
                <div id="slider_next" class="slider_next"></div>
                <div id="slider_prev" class="slider_prev"></div>
                <script type="text/javascript">
                    var $slider_autoplay = <?php if ($autoplay == 1) { echo 'true'; } else { echo 'false'; } ?>;
                    var $slider_time = <?php if (!empty($duration)) { echo $duration . '000'; } else { echo '5000'; } ?>;
                </script>
            </article>