<?php defined( '_JEXEC' ) or die; ?>
<?php

// Get a db connection.
$faqdb = JFactory::getDbo();
 
// Create a new query object.
$query = $faqdb->getQuery(true);
 
$query->select($faqdb->quoteName(array('id','ttstudio_question','ttstudio_answer','ttstudio_orderid','state')));
$query->from($faqdb->quoteName('#__ttstudio_faq'));
$query->order($faqdb->quoteName('ttstudio_orderid'));

// Reset the query using our newly populated query object.
$faqdb->setQuery($query);
$faqresults = $faqdb->loadRowList();

?>

<div class="faq">
    <?php foreach ($faqresults as $row) : ?>
    <?php if ($row[4] == 1) { ?>
    <div class="accordion-header"><?php echo $row[1]; ?></div>
    <div class="accordion-content"><?php echo $row[2]; ?></div>
    <?php } ?>
    <?php endforeach; ?> 
</div>