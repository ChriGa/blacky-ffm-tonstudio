<?php defined( '_JEXEC' ) or die; ?>
<?php

// Get a db connection.
$artistsdb = JFactory::getDbo();
 
// Create a new query object.
$query = $artistsdb->getQuery(true);
 
$query->select($artistsdb->quoteName(array('id','ttstudio_title','ttstudio_desc','ttstudio_image','ttstudio_orderid','state')));
$query->from($artistsdb->quoteName('#__ttstudio_artists'));
$query->order($artistsdb->quoteName('ttstudio_orderid'));

// Reset the query using our newly populated query object.
$artistsdb->setQuery($query);
$artistsresults = $artistsdb->loadRowList();

?>

<ul class="grid">
<?php foreach ($artistsresults as $row) : ?>
<?php if ($row[5] == 1) { ?>
    <li>     
        <figure>
            <div>
                <?php echo '<img src="' . $row[3] . '" alt="" />'; ?>
            </div>
            
            <figcaption>
                <h4><?php echo $row[1]; ?></h4>
                <span><?php echo $row[2]; ?></span>
                <?php
if ($artisthide == 0) {
    echo '<a class="artist' . $row[0] . '" data-rel="artist' . $row[0] . '" href="' . $row[3] . '" data-title="' . $row[1] . '">';
    if (!empty($portfoliotext)) { echo $portfoliotext; } else { echo 'Portfolio'; }
    echo '</a>';
}
                ?>
            </figcaption>
        </figure>         
<script type="text/javascript">
jQuery(function($) {
	$(".artist<?php echo $row[0]; ?>").swipebox({hideBarsDelay : <?php if (!empty($swipeboxautohide)) { echo $swipeboxautohide . '000'; } else { echo '0'; } ?>});
});
</script>
   </li>
<?php } ?>    
<?php endforeach; ?>     
</ul> 

<?php 

// Get a db connection.
$artistsportdb = JFactory::getDbo();
 
// Create a new query object.
$query = $artistsportdb->getQuery(true);
 
$query->select($artistsportdb->quoteName(array('id','ttstudio_title','ttstudio_artist','ttstudio_image','ttstudio_video','ttstudio_orderid','state')));
$query->from($artistsportdb->quoteName('#__ttstudio_artists_portfolios'));
$query->order($artistsportdb->quoteName('ttstudio_orderid'));

// Reset the query using our newly populated query object.
$artistsportdb->setQuery($query);
$artistsportresults = $artistsportdb->loadRowList();

?>

<?php foreach ($artistsportresults as $row) :
if ($row[6] == 1) {
echo '<a data-rel="artist' . $row[2] . '" href="';
if (!empty($row[4])) { echo $row[4]; } else { echo $row[3]; }
echo '" class="hide artist' . $row[2] . '" data-title="' . $row[1] . '"></a>';
}
endforeach; ?> 
