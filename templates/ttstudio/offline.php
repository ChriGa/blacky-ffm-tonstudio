<?php defined( '_JEXEC' ) or die; 

$doc = JFactory::getDocument();
$app = JFactory::getApplication();

$favicon = $this->params->get('ttstudio_favicon');
$customcode = $this->params->get('ttstudio_customcode');
$firstfontlink = $this->params->get('ttstudio_titlefontlink'); 
$secondfontlink = $this->params->get('ttstudio_fontlink');
$headerimage = $this->params->get('ttstudio_logo'); 
$footertext = $this->params->get('ttstudio_footermessage');

$doc->addStyleSheet(JUri::base() . 'templates/system/css/system.css');
$doc->addStyleSheet(JUri::base() . 'templates/' . $this->template . '/css/style.css', $type = 'text/css');
 
$doc->addScript(JUri::base() . 'templates/' . $this->template . '/js/modernizr.custom.js', 'text/javascript');
JHtml::_('jquery.framework', 'text/javascript');
$doc->addScript(JUri::base() . 'templates/' . $this->template . '/js/backstretch.min.js', 'text/javascript');
$doc->addScript(JUri::base() . 'templates/' . $this->template . '/js/toucheffects.js', 'text/javascript');
$doc->addScript(JUri::base() . 'templates/' . $this->template . '/js/jquery.swipebox.min.js', 'text/javascript');

?><!doctype html>
<html class="no-js" lang="<?php echo $this->language; ?>">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <jdoc:include type="head" />
  <link rel="shortcut icon" href="<?php if (!empty($favicon)) { echo $favicon; } else { echo JUri::base() . 'templates/' . $this->template . '/images/favicon.ico'; } ?>" type="image/x-icon" />
    <?php
if (!empty($firstfontlink)) { echo stripslashes($firstfontlink); }
if (!empty($secondfontlink)) { echo stripslashes($secondfontlink); }   
    ?>    
  <?php if (!empty($customcode)) { echo stripslashes($customcode); } ?> 
  <!--[if lte IE 8]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
   <?php require __DIR__ . '/styles.php'; ?>
</head>

<body>   
    <!-- HEADER  -->
    <header>
<?php if (!empty($headerimage)) { echo '<img src="' . $headerimage . '" alt="" />'; } else { echo '<img src="' . JUri::base() . 'templates/' . $this->template . '/images/logo.png" alt="" />'; } ?>
    </header>

<section id="ascensorBuilding">
    <article>
        <div class="page-content-small" data-stellar-ratio="2">         
            <div class="content">
                <h3>
<?php if ($app->getCfg('display_offline_message', 1) == 1 && str_replace(' ', '', $app->getCfg('offline_message')) != ''): ?>
<?php echo $app->getCfg('offline_message'); ?>
<?php elseif ($app->getCfg('display_offline_message', 1) == 2 && str_replace(' ', '', JText::_('JOFFLINE_MESSAGE')) != ''): ?>
<?php echo JText::_('JOFFLINE_MESSAGE'); ?>
<?php endif; ?>
                </h3>
                <hr/>
                        <jdoc:include type="message" />  
                        <form action="<?php echo JRoute::_('index.php', true); ?>" method="post" name="login" id="form-login">
                                <p id="form-login-username">
                                    <label for="username"><?php echo JText::_('JGLOBAL_USERNAME'); ?></label><br />
                                    <input type="text" name="username" id="username" class="inputbox" alt="<?php echo JText::_('JGLOBAL_USERNAME'); ?>" size="18" />
                                </p>
                                <p id="form-login-password">
                                    <label for="passwd"><?php echo JText::_('JGLOBAL_PASSWORD'); ?></label><br />
                                    <input type="password" name="password" id="password" class="inputbox" alt="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>" size="18" />
                                </p>
                                <p id="form-login-remember">
                                    <label for="remember"><?php echo JText::_('JGLOBAL_REMEMBER_ME'); ?></label>
                                    <input type="checkbox" name="remember" value="yes" alt="<?php echo JText::_('JGLOBAL_REMEMBER_ME'); ?>" id="remember" />
                                </p>
                                <p id="form-login-submit">
                                    <input type="submit" name="Submit" class="button" value="<?php echo JText::_('JLOGIN'); ?>" />
                                </p>
                            <input type="hidden" name="option" value="com_users" />
                            <input type="hidden" name="task" value="user.login" />
                            <input type="hidden" name="return" value="<?php echo base64_encode(JURI::base()); ?>" />
                            <?php echo JHTML::_( 'form.token' ); ?>
                        </form>                
            </div>
        </div>
    </article>
</section>

<?php if (!empty($footertext)) { echo '<footer>' . stripslashes($footertext) . '</footer>'; } ?>

<script type="text/javascript" src="<?php echo JUri::base() . 'templates/' . $this->template . '/js/custom2.js' ?>"></script> 
<?php if ($app->getCfg('offline_image')) { echo '<script type="text/javascript"> jQuery("body").backstretch("/' . $app->getCfg('offline_image') . '"); </script>'; } ?>   
</body>

</html>