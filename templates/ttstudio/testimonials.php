<?php defined( '_JEXEC' ) or die; ?>
<?php

// Get a db connection.
$testimonialsdb = JFactory::getDbo();
 
// Create a new query object.
$query = $testimonialsdb->getQuery(true);
 
$query->select($testimonialsdb->quoteName(array('id','ttstudio_name','ttstudio_desc','ttstudio_image','ttstudio_orderid','state')));
$query->from($testimonialsdb->quoteName('#__ttstudio_testimonials'));
$query->order($testimonialsdb->quoteName('ttstudio_orderid'));

// Reset the query using our newly populated query object.
$testimonialsdb->setQuery($query);
$testimonialsresults = $testimonialsdb->loadRowList();

?>

<?php if ($testimonialshide == 0) { ?>
<div class="clear"></div>
<hr/>
<h3><?php if (!empty($testimonialstitle)) { echo $testimonialstitle; } else { echo 'Testimonials'; } ?></h3>
<div class="testimonials">
    <?php foreach ($testimonialsresults as $row) : ?> 
    <?php if ($row[5] == 1) { ?>
    <div class="testimonial-item animated">
        <?php if (!empty($row[3])) { ?>
        <div class="item-left">
            <?php echo '<img src="' . $row[3] . '" alt="" />'; ?>
        </div>
        <?php } ?>   
        <div class="item-<?php if (!empty($row[3])) { echo 'right'; } else {echo 'full';} ?>">
            <p><?php echo $row[2]; ?></p>
            <?php if (!empty($row[1])) {
            echo '<p class="item-client">';
            echo $row[1];
            echo '</p>';
            } ?>
        </div>
    </div>
    <?php } ?>
    <?php endforeach; ?> 
</div>


<script type="text/javascript">
/////////* TESTIMONIALS *///////////
(function($) {
    "use strict";
	$.fn.quovolver = function(speed, delay) {
		if (!speed) { speed = 500; }
		if (!delay) { delay = <?php if (!empty($testimonialsdelay)) { echo $testimonialsdelay; } else { echo '6'; } ?>000; }
		var quaSpd = (speed*4);
		if (quaSpd > (delay)) { delay = quaSpd; }
		var	quote = $(this),
			firstQuo = $(this).filter(':first'),
			lastQuo = $(this).filter(':last'),
			wrapElem = '<div id="quote_wrap"></div>';
		$(this).wrapAll(wrapElem);
		$(this).hide().removeClass('fadeInUp').addClass('fadeOutRight');
		$(firstQuo).show().removeClass('fadeOutRight').addClass('fadeInUp');
		$(this).parent().css({height: $(firstQuo).height()});		
		setInterval(function(){
			if($(lastQuo).is(':visible')) {
				var nextElem = $(firstQuo);
				var wrapHeight = $(nextElem).height();
			} else {
				var nextElem = $(quote).filter(':visible').next();
				var wrapHeight = $(nextElem).height();
			}
			$(quote).filter(':visible').fadeOut(speed).removeClass('fadeInUp').addClass('fadeOutRight');
			setTimeout(function() {
				$(quote).parent().animate({height: wrapHeight}, speed);
			}, speed);
			
			if($(lastQuo).is(':visible')) {
				setTimeout(function() {
					$(firstQuo).fadeIn(speed*2).removeClass('fadeOutRight').addClass('fadeInUp');
				}, speed*2);
				
			} else {
				setTimeout(function() {
					$(nextElem).fadeIn(speed).removeClass('fadeOutRight').addClass('fadeInUp');
				}, speed*2);
			}
			
		}, delay);
	
	};
})(jQuery);

jQuery(window).load(function () { 
    "use strict";
    jQuery('.testimonial-item').quovolver(); 
});
</script>

<?php } ?>